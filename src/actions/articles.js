import { FETCHING_ARTICLES, FETCHING_ARTICLES_SUCCESS, FETCHING_ARTICLES_FAILURE, GET_ARTICLE } from './constants';
import axios from 'axios';
export default getArticlesFromAPI = () => {
    return (dispatch) => {
        dispatch(getArticles());
        axios.get('https://api.myjson.com/bins/dvmar')
            .then(res => dispatch(getArticlesSuccess(res.data)))
            .catch(err => dispatch(getArticlesFailure(err)));
    }
}

getArticles = () => {
    return {
        type: FETCHING_ARTICLES
    }
}

getArticlesSuccess = (data) => {
    return {
        type: FETCHING_ARTICLES_SUCCESS,
        data,
    }
}

getArticlesFailure = () => {
    return {
        type: FETCHING_ARTICLES_FAILURE
    }
}