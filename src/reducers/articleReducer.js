import { FETCHING_ARTICLES, FETCHING_ARTICLES_SUCCESS, FETCHING_ARTICLES_FAILURE, GET_ARTICLE } from '../actions/constants';


const initialState = {
    articles: {},
    isLoading: false,
    error: false,
}

ArticleReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCHING_ARTICLES: {
            return {
                ...state,
                isLoading: true,
                articles: {},
            }
        }

        case FETCHING_ARTICLES_SUCCESS: {
            return {
                ...state,
                isLoading: false,
                articles: action.data,
            }
        }

        case FETCHING_ARTICLES_FAILURE: {
            return {
                ...state,
                isLoading: false,
                error: true,
            }
        }

        default:
            return state;


    }
}
export default ArticleReducer;