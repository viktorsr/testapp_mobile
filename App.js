import React, { Component } from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import configureStore from './src/store/index';

const store = configureStore();

import NewsScreen from './screens/NewsScreen';
import ArticleScreen from './screens/ArticleScreen';

const MainNavigator = createAppContainer(createStackNavigator(
	{
		News: {
			screen: NewsScreen,
			navigationOptions: {
				header: null,
			},
		},
		Article: ArticleScreen,
	},
	{
		initialRouteName: 'News',
	}
));
class App extends Component {
	render () {
		return (
			<Provider store={store}>
				<MainNavigator />
			</Provider>
		);
	}

}

export default App;
