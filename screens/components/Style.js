import { StyleSheet, Dimensions } from 'react-native';
const win = Dimensions.get('window');

const styles = StyleSheet.create({

    main: {
        backgroundColor: 'whitesmoke',
        flex: 1,
    },

    image: {
        width: win.width,
        height: 450 * win.width / 800,
    },

    articleTitle: {
        color: 'black',
        fontSize: 22,
    },

    articleDescriptions: {
        color: 'gray',
        fontSize: 18,
    },

    article: {
        backgroundColor: 'white',
        marginVertical: 10,
        paddingBottom: 5,
    },

    articleText: {
        paddingHorizontal: 5,
    },

    articleScreen: {
        backgroundColor: 'white',
        paddingVertical: 5,
    },

    title: {
        color: 'black',
        fontSize: 26,
    },

    description: {
        color: 'gray',
        fontSize: 20,
    },

});

export default styles;