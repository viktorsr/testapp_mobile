import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from 'react-navigation';
import styles from './Style';

class Article extends Component {
    constructor (props) {
        super(props);
    }

    openArticle = (id) => {
        console.log(id);
        this.props.navigation.navigate('Article', { id })
    }

    render () {
        const { title, description, image, type } = this.props.article;
        const { index } = this.props
        return (
            <TouchableOpacity onPress={() => { this.openArticle(index) }}>
                <View style={styles.article}>
                    {type == 'TEXT' ?
                        null
                        :
                        <Image
                            source={{ uri: image }}
                            style={styles.image}
                        />
                    }
                    <View style={styles.articleText}>
                        <Text style={styles.articleTitle}> {title} </Text>
                        <Text style={styles.articleDescription}> {description} </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(Article);