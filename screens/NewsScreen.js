import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator, Button } from 'react-native';
import getArticlesFromAPI from '../src/actions/articles';
import { connect } from 'react-redux';

import Article from './components/Article';
import styles from './components/Style';


class NewsScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };
        this.props.getArticles();
    }


    renderArticle ({ item, index }) {
        return (
            <Article
                article={item}
                index={index}
            />
        );
    }

    render () {
        return (
            <View style={styles.main}>
                {this.props.isLoading ?
                    <ActivityIndicator size="large"></ActivityIndicator>
                    :
                    <FlatList
                        data={this.props.articles}
                        keyExtractor={(item, index) => item._id}
                        renderItem={this.renderArticle}
                    />
                }

            </View>
        );
    }

}



const mapStateToProps = (state) => {
    const articles = state.article.articles;
    const isLoading = state.article.isLoading;
    console.log(articles);
    return { articles, isLoading }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getArticles: () => dispatch(getArticlesFromAPI())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);
