import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';

import styles from './components/Style';

class ArticleScreen extends Component {
    constructor (props) {
        super(props);
        this.state = {
        };

    }

    render () {
        const { image, type, title, description } = this.props.articles[this.props.navigation.state.params.id]

        return (
            <View style={styles.articleScreen}>
                {type == 'TEXT' ?
                    null
                    :
                    <Image
                        source={{ uri: image }}
                        style={styles.image}
                    />
                }
                <View style={styles.articleText}>
                    <Text style={styles.title}> {title} </Text>
                    <Text style={styles.description}> {description} </Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const articles = state.article.articles;
    return { articles }
}

export default connect(mapStateToProps)(ArticleScreen);
